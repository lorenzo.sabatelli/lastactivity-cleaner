@ECHO OFF
CLS

ECHO.
ECHO  Execute as administrator. (required)
ECHO.
timeout 30
cls

ECHO.
ECHO _______________________________________________________________________________
ECHO ```````````````````````````````````````````````````````````````````````````````
ECHO  # Clean of: 
ECHO    - User Logon 
ECHO    - User Logoff 
ECHO    - Windows Installer Started 
ECHO    - Windows Installer Ended 
ECHO    - System Started, System Shutdown 
ECHO    - Resumed from sleep 
ECHO    - Restore Point Created 
ECHO    - Network Connected 
ECHO    - Network Disconnected 
ECHO    - Software Crash 
ECHO    - Software stopped responding (hang)
ECHO.	
ECHO.

for /f %%a in ('WEVTUTIL EL') do (
     WEVTUTIL CL "%%a" 2>null
     echo log removed: "%%a"
)

ECHO.
ECHO.
ECHO  Done.
ECHO.
ECHO.
ECHO _______________________________________________________________________________
ECHO ```````````````````````````````````````````````````````````````````````````````
ECHO  # Clean 'Run .EXE file'
ECHO.
ECHO.

del C:\Windows\Prefetch\*.pf

ECHO.
ECHO.
ECHO  Done.
ECHO.
ECHO.
ECHO _______________________________________________________________________________
ECHO ```````````````````````````````````````````````````````````````````````````````
ECHO  # Clean 'View Folder in Explorer'
ECHO.
ECHO.


:: Created by: Shawn Brink
:: http://www.sevenforums.com
:: Tutorial:  http://www.sevenforums.com/tutorials/15692-folder-view-settings-reset-all-default.html


Reg Delete "HKCU\Software\Microsoft\Windows\Shell\BagMRU" /F
Reg Delete "HKCU\Software\Microsoft\Windows\Shell\Bags" /F
Reg Delete "HKCU\Software\Microsoft\Windows\ShellNoRoam\Bags" /F
Reg Delete "HKCU\Software\Microsoft\Windows\ShellNoRoam\BagMRU" /F

Reg Delete "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\BagMRU" /F
Reg Delete "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\Bags" /F
Reg Delete "HKCU\Software\Classes\Wow6432Node\Local Settings\Software\Microsoft\Windows\Shell\Bags" /F
Reg Delete "HKCU\Software\Classes\Wow6432Node\Local Settings\Software\Microsoft\Windows\Shell\BagMRU" /F



ECHO.
ECHO.
ECHO  Done.
ECHO.
ECHO.
ECHO _______________________________________________________________________________
ECHO ```````````````````````````````````````````````````````````````````````````````
ECHO  # Clean 'Open file or folder'
ECHO.
ECHO.

::REG IMPORT cri.reg
del /Q %appdata%\Microsoft\Windows\Recent\*

ECHO.
ECHO.
ECHO  Done.
ECHO.
ECHO.
ECHO _______________________________________________________________________________
ECHO ```````````````````````````````````````````````````````````````````````````````
ECHO  # Clean 'Select file in open/save dialog-box'
ECHO.
ECHO.

::url: http://www.sevenforums.com/general-discussion/208005-residual-file-names-save-save-dialog-boxes.html

Reg Delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\OpenSavePidlMRU" /F

ECHO.
ECHO.
ECHO  Done.
ECHO.
ECHO.
ECHO  Completed.
ECHO.
pause